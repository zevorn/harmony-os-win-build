# HarmonyOS-Win-Build

### 介绍

本仓包含了构建HarmonyOS2.0在window上面编译的运行环境，硬件平台为Hi3861。目前仅支持轻量型HarmonyOS，后续根据HarmonyOS的更新，增加其他系统的编译构建环境。



由于HarmonyOS的编译工具链主要以linux下的开发为主，因此在windows上实现对HarmonyOS的编译，需要借助MinGW的msys，提供一些运行环境的支持条件。同时，HarmonyOS2.0的轻量型系统，已经对windows开发环境做了适配，给我们的环境搭建提供了工程基础。


欢迎大家赏个`Star`，我会持续更新打包分享HarmonyOS在win上的开发环境。

### 步骤



1. 需要借助dev-tool，完成工程开发，请访问下面的链接，完成dev-tool环境的搭建：

   [dev-tool搭建教程](https://device.harmonyos.com/cn/docs/ide/user-guides/install_windows-0000001050164976)

2. 在完成dev-tool的环境搭建以后，我们需要准备好可以编译HarmonyOS的工具链：

   [windows下编译所需工具链安装教程](https://device.harmonyos.com/cn/docs/ide/user-guides/hi3861_windows-0000001101110444)



### 目录结构



本仓已经为大家准备好了所需要的工具链，可以下载以后，按照上面链接里的教程，将工具链的地址导入dev-tool当中，即可完成工程的快速编译。本仓的目录结构如下：

| 一级目录 | 二级目录 | 说明                |
| -------- | -------- | ------------------- |
| MinGW    | -     | 存放msys工具        |
| Hi3861   | tools    | 存放gn、ninja等工具 |



### 常见问题

1. python的版本为V3.7.4~V3.8.x 64位版本，32位版本不可以。当电脑安装多个python版本的时候，要保证在这个版本范围的python具有优先执行权。
2. dev-tool的工具链安装，必须要和官方教程里给出的工具名称一致，不支持自定义命名，不然会编译错误。